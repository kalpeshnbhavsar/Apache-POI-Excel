package dropdownCellValidationDemo_2;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class Main {

    static final String FOLDER_PATH = "." + File.separator + "dropdownCellValidationDemo_2" + File.separator;
    static final String FILE_PATH = FOLDER_PATH + "XLValidationFrmCells.xls";

    public static void main(String[] args) {
        try {
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.createSheet("Data Validation");
            CellRangeAddressList addressList = new CellRangeAddressList(0, 0, 0, 0);
            DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("$B$1:$H$1");
            DataValidation dataValidation = new HSSFDataValidation(addressList, dvConstraint);
            dataValidation.setSuppressDropDownArrow(false);
            sheet.addValidationData(dataValidation);

            fileChecker(FILE_PATH);

            FileOutputStream fileOut = new FileOutputStream(FILE_PATH);

            workbook.write(fileOut);
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean folderChecker(String folderPath) {
        File fileDir = new File(folderPath);
        return !new File(folderPath).exists() && fileDir.mkdir();
    }

    private static boolean fileChecker(String filePath) {
        File file = new File(filePath);
        folderChecker(file.getParent());        //get folder
        if (!file.exists())
            try {
                return file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        else return false;

    }

}
