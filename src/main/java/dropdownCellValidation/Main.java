package dropdownCellValidation;

import java.io.FileOutputStream;

import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;


public class Main {

    public static void main(String[] args) {
        try {
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.createSheet("Data Validation");
            CellRangeAddressList addressList = new CellRangeAddressList(0, 0, 0, 0);
            DVConstraint dvConstraint = DVConstraint.createFormulaListConstraint("$B$1:$H$1");
            DataValidation dataValidation = new HSSFDataValidation(addressList,dvConstraint);
            dataValidation.setSuppressDropDownArrow(false);
            sheet.addValidationData(dataValidation);
            FileOutputStream fileOut = new FileOutputStream("XLValidationFrmCells.xls");

            workbook.write(fileOut);
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
